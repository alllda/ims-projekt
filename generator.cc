#include "simlib.h"
#include "global.h"
#include "order.h"
#include "generator.h"
#include <iostream>

HomeGenerator::HomeGenerator(){

}

void HomeGenerator::Behavior(){
	// Main time 12PM to 2 AM
	if(Time >= 0 && Time < HOME_ORDERS_TIME){
		double nextTime = Normal((HOME_ORDERS_TIME)/ HOME_BAKERY_COUNT, HOUR * 0.1);
		// in case of negative value
		if(nextTime < 0){
			nextTime = 1;
		}
		// max HOMEBAKERYCOUNT per main time
		if(id <= HOME_BAKERY_COUNT - 1){
			int count = (COUNT_PER_DAY * HOME_RATIO) / HOME_BAKERY_COUNT;

			(new Order(count,HOME_PRIO))->Activate(Time);
			std::cout << "[Main]Home:   " << Time << " - ID: " << id++ << " count: " << count << std::endl;
		}
		Activate(Time + nextTime);

		
	}
	// break time
	else if(Time >= HOME_ORDERS_TIME && Time < HOUR * 15){
		// Income orders are ignored
		Activate(HOUR * 15 + Uniform(0, 0.5*HOUR));
	}
	else if(Time >= HOUR *15 && Time < 22 * HOUR){ // 3AM to 10:30AM
		double nextTime = Normal(ADITIONAL_ORDER_TIME/ADITIONAL_ORDER_RATE, HOUR * 0.5);
		// in case of negative value
		if(nextTime < 0)
			nextTime = 1;
		// 20 - 50 % of main time order
		int count = (COUNT_PER_DAY * HOME_RATIO) / HOME_BAKERY_COUNT * Uniform(0.2,0.5);
		std::cout << "[Aditional]Home:   " << Time << " - ID: " << id++ << " count: " << count << std::endl;
		(new Order(count,HOME_PRIO))->Activate(Time);
		Activate(Time + nextTime);

	}

}



OtherGenerator::OtherGenerator(){

}

void OtherGenerator::Behavior(){
	// Main time 12PM to 5 PM
	if(Time >= 0 && Time < OTHER_ORDERS_TIME){		
		double nextTime = Normal((OTHER_ORDERS_TIME)/ OTHERS_COUNT, HOUR * 0.1);
		// in case of negative value
		if(nextTime < 0){
			nextTime = 1;
		}
		// max OTHERSCOUT per main time
		if(id <= OTHERS_COUNT -1){
			int count = (COUNT_PER_DAY * OTHERS_RATIO) / OTHERS_COUNT;
			std::cout <<"[Main]Others: " << Time << " - ID: " << id++ << " count: " << count << std::endl;
			(new Order(count,OTHERS_PRIO))->Activate(Time);
		}
		Activate(Time + nextTime);

		
	}
	// break time - orders are ignored
	else if(Time >= OTHER_ORDERS_TIME && Time < HOUR * 15){
		Activate(HOUR * 15 + Uniform(0, 0.5*HOUR));
	}
	else if(Time >= HOUR *15 && Time < 22 * HOUR){ // 3AM to 10:30AM
		double nextTime = Normal(ADITIONAL_ORDER_TIME/ADITIONAL_ORDER_RATE, HOUR * 0.5);
		if(nextTime < 0)
			nextTime = 1;
		// 20% - 50% of main time order
		int count = (COUNT_PER_DAY * OTHERS_RATIO) / OTHERS_COUNT * Uniform(0.2,0.5);
		std::cout << "[Aditional]Other: " << Time << " - ID: " << id++ << " count: " << count << std::endl;
		(new Order(count,OTHERS_PRIO))->Activate(Time);
		Activate(Time + nextTime);
	}
}