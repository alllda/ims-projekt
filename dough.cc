#include "dough.h"
#include "global.h"
#include <iostream>
#include "croissant.h"

Dough::Dough(int count){
	this->count = count;
}

void Dough::Behavior(){
	// Seize divider machine
	Seize(Divider);
	double t = Time;
	// generate all peaces from dough
	while(this->count != 0){
		// generate new croissant at each line
		for(int i = 0 ; i < DIVIDER_LINES && this->count != 0; i++){
			(new Croissant())->Activate(Time);
			this->count--;	
		}
		// one take 0.5s ... 6 lines * 0.5 = 3s
		Wait(3);
	}
	Release(Divider);
	

}