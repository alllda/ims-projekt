#include "simlib.h"
#include "order.h"
#include "global.h"
#include "generator.h"
#include "cleaner.h"
#include <iostream>
#include <time.h> 



// global objects
Histogram OrderHist("Incoming orders",0,HOUR,24);
Facility Mixer("Mixer");
Facility Divider("Divider");
Store ShapingMachine("ShapingMachine",5);
Store Furnace("Furnace",3000);
Histogram CroissantTime("Croissant Time",3200,50,14);
Histogram CroissantDonePerHour("Croissant done per hour",0,HOUR,24);


int main() {                 
  Print("Bakery - IMS project - SIMLIB/C++\n");
  SetOutput("out/model.out");

  // Set random seed for generators
  RandomSeed(time(NULL));
  // One day simulation
  Init(0,DAY);              
  (new HomeGenerator())->Activate(); 		// Home orders generator
  (new OtherGenerator())->Activate();		// Other orders generator
  (new Cleaner(CLEANER_PRIO))->Activate(); 	// Activte cleaning process
  Run();                     

  // Print statistic to output file
  OrderHist.Output();
  Mixer.Output();              
  Divider.Output();
  ShapingMachine.Output();
  Furnace.Output();
  CroissantTime.Output();
  CroissantDonePerHour.Output();

  return 0;
}
