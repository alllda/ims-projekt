CPPFLAGS=-std=c++11 -pedantic -Wall 

LDFLAGS = -lsimlib -lm

OBJFILE = order.o generator.o croissant.o dough.o cleaner.o

imsSim: imsSim.cc $(OBJFILE) global.h
	g++ $(CPPFLAGS) imsSim.cc $(OBJFILE) -o $@ $(LDFLAGS)

order.o: order.cc order.h global.h
	g++ $(CPPFLAGS) -c order.cc

generator.o: generator.cc generator.h global.h
	g++ $(CPPFLAGS) -c generator.cc

croissant.o: croissant.cc croissant.h global.h
	g++ $(CPPFLAGS) -c croissant.cc

dough.o: dough.cc dough.h global.h
	g++ $(CPPFLAGS) -c dough.cc

cleaner.o: cleaner.cc cleaner.h global.h
	g++ $(CPPFLAGS) -c cleaner.cc


auto: imsSim
	./imsSim

graph: auto
	python histFilter.py > tmp.txt
	python graphGenerator.py tmp.txt
	rm tmp.txt


clean:
	rm -rf imsSim
	rm -rf *.o

cleanAll:	clean
	rm  out/*
	rm out/graph/*