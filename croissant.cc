#include "croissant.h"
#include "global.h"
#include <iostream>

Croissant::Croissant(){

}

void Croissant::Behavior(){
	double income = Time;

	Wait(2); // flour

	// pre-breaded
	Wait(Uniform(7*MIN,9*MIN));

	Enter(ShapingMachine);
	Wait(Exponential(2));
	Leave(ShapingMachine);

	// breaded
	Wait(Uniform(35*MIN,40*MIN));
	// water
	Wait(Exponential(3));

	// bakeing
	Enter(Furnace);
	Wait(Uniform(12*MIN,13*MIN));
	Leave(Furnace);

	// statistics
	CroissantTime(Time - income);
	CroissantDonePerHour(Time);

}