#ifndef _cleaner
#define _cleaner

#include "simlib.h"

class Cleaner : public Process
{
public:
	Cleaner(int priority);
	void Behavior();
	
};

#endif