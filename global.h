#ifndef _GLOB
#define _GLOB


// time 
#define SEC 1
#define MIN 60*SEC
#define HOUR 60*MIN
#define DAY 24*HOUR

// global objects
extern Facility Mixer;
extern Facility Divider;
extern Store ShapingMachine;
extern Store Furnace;
extern Histogram OrderHist;
extern Histogram CroissantTime;
extern Histogram CroissantDonePerHour;

// count of croissant per main orders
#define COUNT_PER_DAY 60997

#define BATCH 300 // 300kg per batch
#define CROISSANT_WEIGTH 0.05 // one croissant weighs 50g
#define CROISSANT_PER_BATCH BATCH/CROISSANT_WEIGTH

// order time
#define HOME_ORDERS_TIME HOUR*14
#define OTHER_ORDERS_TIME HOUR*5
#define ADITIONAL_ORDER_TIME HOUR*8
#define ADITIONAL_ORDER_RATE 1/6.0
#define HTIME_ORDERS OTHER_ORDERS_TIME/(float)18

#define HOME_BAKERY_COUNT 18
#define OTHERS_COUNT 10

// 50:50
#define HOME_RATIO 0.5
#define OTHERS_RATIO 0.5

// priority
#define CLEANER_PRIO 3
#define HOME_PRIO 2
#define OTHERS_PRIO 1

#define DIVIDER_LINES 6

#endif