#ifndef _DOUGH
#define _DOUGH

#include "simlib.h"

class Dough : public Process
{
public:
	Dough(int count);
	void Behavior();

private:
	int count;
};


#endif