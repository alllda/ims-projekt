#ifndef _ORDER
#define _ORDER

#include "simlib.h"

class Order : public Process
{
public:
	Order(int count, int priority);
	void Behavior();

private:
	int count;
	int priority;
	int done;
	
};


#endif