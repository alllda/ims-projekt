import sys
import re

'''
Description:
	script read data from out/model.out and print raw 
	data of	histograms to 3 column:
		from 	to 		value

	columns are delimited by tabulator
Usage:
	python histFilter.py arguments

	arguments:
		none - print all histograms
		otherwise - print specific histogram by name 
'''

def parseHist(histogram):
	lines = histogram.splitlines()
	#print histogram
	#print ""
	#print "RAW data"
	name = lines[1].split('|')[1].split("HISTOGRAM")[1].strip()
	print name
	print "From\tTo\tValue"
	for line in lines[12:-1]:
		item = line.split("|")
		for i in item[1:-3]:
			val = i.strip()
			if "." in val:
				splited = val.split(".")
				if(splited[1] == "000"):
					val = splited[0]
				else:
					val = splited[0]+"."+splited[1][0]

			print val + "\t",
		print ""
	print "-------------------------------------------"

def printHist(histogram):
	if len(sys.argv) == 1:
		parseHist(histogram)
	else:
		for arg in sys.argv:
			if histogram.startswith("+----------------------------------------------------------+\n| HISTOGRAM "+arg):
				parseHist(histogram)


file = open("out/model.out").read()
file.replace("\r\n","\n")

list = re.split("\+\n\n\+",file)

countstart = 0
coutnend = 0
for item in list:
	item = item.strip()
	if item[0] != "+":
		item = "+"+item
		countstart += 1
	if item[-1]  != "+":
		item = item+"+"
		coutnend += 1
	if(item.startswith("+----------------------------------------------------------+\n| HISTOGRAM")):
		printHist(item)




