#include "order.h"
#include <iostream>
#include "global.h"
#include "croissant.h"
#include "dough.h"

Order::Order(int count, int priority){
	this->Priority = priority;
	this->count = count;
}

void Order::Behavior() {
    // remember income time
    OrderHist(Time);
    int done = 0;

    // batch count     
    int loopNum =(int)(this->count / (BATCH / CROISSANT_WEIGTH));
    if(loopNum * (BATCH / CROISSANT_WEIGTH) > 0 || loopNum == 0)
    	loopNum++;

    Seize(Mixer);
    int croissantMake;
    for(int i = 0; i < loopNum; i++){
        // prepare time
    	Wait(Normal(5*MIN,0.25*MIN));
    	Wait(Normal(11*MIN,0.25*MIN));
    	if(done + CROISSANT_PER_BATCH < this->count){
    		done += CROISSANT_PER_BATCH;
    		croissantMake = CROISSANT_PER_BATCH;
    	}
    	else{
    		croissantMake = this->count - done;
    	}
        // generate new dough
    	(new Dough(croissantMake))->Activate();
    }
    
    Release(Mixer);
    
  }

