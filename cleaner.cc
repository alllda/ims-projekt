#include "cleaner.h"
#include "global.h"
#include "simlib.h"

Cleaner::Cleaner(int priority){
	this->Priority = priority;	// greatest priority, always take facility

}

void Cleaner::Behavior(){
	Seize(Mixer);
	Wait(5*HOUR); 		// clean machine takes 5 hour
	Release(Mixer);
	Activate(19*HOUR);	// another in 19 hours
}