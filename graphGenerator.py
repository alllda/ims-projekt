import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys


def generateHistogram(data):
	print data
	lines = data.splitlines()
	name = lines[0].strip()
	min = float(lines[2].split('\t')[0])
	max = float(lines[-1].split('\t')[1])
	n = len(lines)-2
	values = []
	for l in lines[2:]:		
		colMin = float(l.split("\t")[0])
		colMax = float(l.split("\t")[1])
		m = (colMax + colMin) /2	
		v = int(l.split("\t")[2])
		for _ in range(v):
			values.append(m)
		values.append(v)

	plt.hist(values,n,range=(min,max),histtype='barstacked',facecolor='blue')
	plt.xlabel('Time [s]')
	plt.ylabel('Count [pc]')
	plt.title(r'Histogram of '+name)
	plt.show()
	plt.savefig("out/graph/"+name+'.png', dpi=100)


file = open(sys.argv[1],'r').read()

file = file.split("-------------------------------------------\n")[:-1]

for data in file:
	generateHistogram(data)

